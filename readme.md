# Liam's Dotfiles for NixOS

## Includes configurations for:
- alacritty
- bspwm
- btop
- cava
- conky
- dunst
- neovim
- picom
- polybar
- [rofi](https://github.com/adi1090x/rofi)
- starship
- sxhkd
- zathura
- zsh
