# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Firmware
  hardware.enableAllFirmware = true;

  # Bootloader.
  #boot.loader.systemd-boot.enable = true;
  #boot.loader.efi.canTouchEfiVariables = true;
  #boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      efiSupport = true;
      device = "nodev";
    };
  };

  networking.hostName = "olympus"; # Define your hostname.
  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Brisbane";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_AU.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_AU.UTF-8";
    LC_IDENTIFICATION = "en_AU.UTF-8";
    LC_MEASUREMENT = "en_AU.UTF-8";
    LC_MONETARY = "en_AU.UTF-8";
    LC_NAME = "en_AU.UTF-8";
    LC_NUMERIC = "en_AU.UTF-8";
    LC_PAPER = "en_AU.UTF-8";
    LC_TELEPHONE = "en_AU.UTF-8";
    LC_TIME = "en_AU.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
    enable = true;
    displayManager = {
      lightdm = {
	enable = true;
      };
      defaultSession = "none+bspwm";
    };
    windowManager.bspwm.enable = true;
    excludePackages = [ pkgs.xterm ];
  };
  
  # Audio
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  nixpkgs.config.pulseaudio = true;

  # Spotify
  #systemd.user.services.spotifyd = {
  #  enable = true;
  #  serviceConfig = {
  #    ExecStart = "${pkgs.spotifyd}/bin/spotifyd --config-path /home/crispy/.config/spotifyd/spotifyd.conf --no-daemon";
  #    Restart = "always";
  #  };
  #};

  # Fonts
  fonts.fonts = with pkgs; [
    fira-code
    fira-code-symbols
    material-design-icons
    (nerdfonts.override {fonts = [ "FiraCode" ]; })
    raleway
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.crispy = {
    isNormalUser = true;
    description = "liam";
    extraGroups = [ "networkmanager" "wheel" "audio" ];
    packages = with pkgs; [];
  };

  # zsh
  programs.zsh.enable = true;
  users.defaultUserShell = with pkgs; zsh;
  environment.shells = with pkgs; [ zsh ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    
    # Core
    git
    psmisc

    # For window manager (bspwm)
    sxhkd
    polybar
    rofi
    alacritty
    nitrogen
    i3lock-color
    picom
    conky
    dunst
  
    # Shell config
    starship

    # GTK Stuff (awful)
    lxappearance
    qogir-theme

    # Editors
    neovim

    # Browsers
    firefox
    librewolf
    tor-browser-bundle-bin

    # Torrent clients
    deluge

    # PDF, EPUB readers
    zathura

    # Video
    vlc

    # Audio
    spotifyd
    spotify-tui

    # Terminal apps
    exa
    ranger
    ripgrep
    fd
    macchanger
    scrot
    xclip
    btop
    redshift

    # Fancy fetch apps
    pfetch
    freshfetch
    neofetch
  
    # Eye candy term apps
    tty-clock
    bklk
    pipes-rs
    cmatrix
    cava
    globle-cli
    cbonsai

    # Terminal games
    nudoku

    # Misc libs
    libpulseaudio
    libnotify
    playerctl
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}
