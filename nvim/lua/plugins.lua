return require('packer').startup(function(use)
  
  -- Packer
  use('wbthomason/packer.nvim')
  
  -- File tree
  use({
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons',
    },
    config = function()
      require('nvim-tree').setup()
      --require('nvim-tree.api').tree.open()
    end,
  })
  
  -- Status bar
  use({
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = function()
      require('lualine').setup({
        options = {
          theme = 'onedark',
        },
      })
    end,
  })
  
  -- Tab bar
  use({
    'romgrk/barbar.nvim',
    requires = {
      'nvim-tree/nvim-web-devicons'
    },
  })
  
  -- Win bar
  use({
    'utilyre/barbecue.nvim',
    tag = "*",
    requires = {
      'SmiteshP/nvim-navic',
      'nvim-tree/nvim-web-devicons',
    },
    config = function()
      require('barbecue').setup()
    end,
  })
  
  -- Line highlight
  use({
    'mawkler/modicator.nvim',
    after = 'onedark.nvim',
    config = function()
      require('modicator').setup({
        show_warnings = false,
      })
    end,
  })
  
  -- Dashboard
  use({
    'startup-nvim/startup.nvim',
    requires = {
      'nvim-telescope/telescope.nvim',
      'nvim-lua/plenary.nvim'
    },
    config = function()
      require('startup').setup()
    end,
  })
  
  -- Language server
  use({
    'neovim/nvim-lspconfig',
    requires = {
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
      'j-hui/fidget.nvim',
      'folke/neodev.nvim',
    },
    config = function()
      require('mason').setup()
      require('mason-lspconfig')
    end
  })

  use({
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',
    },
    config = function()
      require('cmp').setup({
        { name = 'nvim_lsp' },
        { name = 'luasnip' }
      })
    end,
  })

  -- Treesitter
  use({
    'nvim-treesitter/nvim-treesitter',
    config = function()
      require('nvim-treesitter.configs').setup({
        ensure_installed = { "c", "lua" },
        sync_install = false,
        auto_install = true,
        ignore_install = { "javascript" },
        highlight = {
          enable = true,
        },
      })
    end,
  })
  
  -- Git
  --use({'f-person/git-blame.nvim',})
  use('gitgutter/Vim')
  use('tpope/vim-fugitive')
  
  -- Util
  use({'jghauser/mkdir.nvim'})  
  
  use({
    'nvim-telescope/telescope.nvim',
    tag = '0.1.1',
    requires = {
      'nvim-lua/plenary.nvim',
    },
  })
  
  use({
    'lewis6991/impatient.nvim',
    config = function()
      require('impatient')
    end,
  })
  
  use({
    'windwp/nvim-autopairs',
    config = function()
      require('nvim-autopairs').setup({})
    end
  })

  use('lukas-reineke/indent-blankline.nvim')

  use('tpope/vim-sleuth')

  use({
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end,
  })
  --use('Yggdroot/indentLine')
  
  -- Theming
  use({
    'navarasu/onedark.nvim',
    config = function()
      require('onedark').load()
    end,
  })  
end)
