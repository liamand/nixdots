-- Variables for QoL
local g = vim.g
local o = vim.o

-- Line numbering
o.number = true
o.relativenumber = true
o.numberwidth = 5

-- Syntax
g.syntax = true

-- Tabs
o.expandtab = true
o.smartindent = true
o.tabstop = 2
o.shiftwidth = 2

-- Misc
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1
o.termguicolors = true
o.cursorline = true
o.showmode = false
